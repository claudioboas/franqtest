# Franq Test


This project was created with the goal to evaluate my technical skills in relation of the basic concepts of object orientation and
domain over API structures. It was designed as I went researching about the staack recommended,
reading and completing the challenge items.
## Development Staack
	
- [Django 3.1.5](https://www.djangoproject.com/)

- [Python 3.9.1](https://www.python.org/downloads/release/python-391/)
	
- [PostGres  13.1](https://www.postgresql.org/docs/13/release-13-1.html)

## Project dependencies


 `asgiref==3.3.1`
- `certifi==2020.12.5`
- `chardet==4.0.0`
- `Django==3.1.5`
- `djangorestframework==3.12.2`
- `idna==2.10`
- `psycopg2==2.8.6`
- `pytz==2020.5`
- `requests==2.25.1`
- `sqlparse==0.4.1`
- `urllib3==1.26.2`

### Or it's possible to run `pip install -r requirements.txt`


## Development

It was used MacOS Catalina - version 10.15.7


### Database configurations at settings.py:

```json
'NAME': 'franqtest'
'USER': 'claudiovilasboas'
'PASSWORD': 'password'
'PORT': '5432'
```

#### To run locally add this database to postgres with the username and password above or change it at settings.py
#### Endpoints were configured to access api at port 5000 (python3 manage.py runserver 0.0.0.0:5000)

   

## Live Demo

- The server used was a  EC2 instance from aws with ubuntu 18.04 image 

### Link For DEMO

[http://ec2-54-194-18-191.eu-west-1.compute.amazonaws.com:5000](http://ec2-54-194-18-191.eu-west-1.compute.amazonaws.com:5000)

to checkout django admin panel use:

	superuser: superuser

	password: senha

Or simply create a new admin user on register

### To see API for thirdparty users:

[http://ec2-54-194-18-191.eu-west-1.compute.amazonaws.com:5000/getUsersAndGarages](http://ec2-54-194-18-191.eu-west-1.compute.amazonaws.com:5000)

It wasn't developed any authentification token to access this endpoint for simplicity reasons

### Exercises Developed ###
All exercises were done.
