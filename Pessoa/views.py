from django.shortcuts import render , redirect
from django.http import HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from . import models
import requests
import json
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import UserSerializer

#Uncomment print to checkout the console logs used to develop the application

def index(request):

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username = username, password =password  )

        if user is not None:
            auth.login(request , user)
            return redirect('/home')    
        else:
            messages.info(request, 'invalid username or password')
            return redirect("/")
    else:
        return render(request,'index.html')


def register(request):

    if request.method == 'POST':
        #garage api url
        url = 'http://localhost:5000/api/garage-create/'
        print(request)
        email = request.POST['email']
        username = request.POST['username']
        password= request.POST['password']
        number = request.POST['cellPhone']
        cellphone = {'number':number}
        extra_fields = {
            'cellphone': number,
            'is_staff': False
        }
        is_admin_user = False
        if 'adminUser' in request.POST.keys():
            is_admin_user = True
        
        if(models.User.objects.filter(username=username).exists()):
            return render(request, 'register.html', {'some_flag': True,'alert': 'User already exists, please choose another name'})
        elif(models.User.objects.filter(email=email).exists()):
            return render(request, 'register.html', {'some_flag': True, 'alert': 'Email is already registered'})
        else:
            user = models.User.objects.create_user(username = username , password = password , email = email, phone=number,is_staff=is_admin_user)
            user.save()
            print('user created')
            #Sending data to garage api
            post_data = {'email': email, 'cellPhone': number, 'adminUser':is_admin_user}
            result = requests.post(url, data = post_data)
            return render(request, 'register.html', {'success_flag': True})

    return render(request,'register.html')


def home(request):
    url_vehiclesList = 'http://localhost:5000/api/vehicles-list/'
    url_updategatage = 'http://localhost:5000/api/update-garage/'
    url_getUserGarage = 'http://localhost:5000/api/user-garage/'
    
    #Getting all vehicles list
    result = requests.get(url_vehiclesList)
    vehiclemodels = result.json().get('vehicles')
    colorlist = result.json().get('colors')
    carslist = list(filter(lambda d: (d.get("type",None) == 'car'),vehiclemodels))
    motorcyclelist = list(filter(lambda d: (d.get("type",None) == 'motorcycle'),vehiclemodels))
    logged_in_user = auth.get_user(request)
    user_email = models.User.objects.filter(username=logged_in_user).values_list('email', flat=True)
    if request.method == 'POST':
        post_data = {'email': user_email}
        if 'selected_cars' in request.POST.keys():
            selected_cars = []
            colors = request.POST.getlist('colors')
            # print(colors)
            err = 1
            for ids in request.POST.getlist('selected_cars'):
                for car in carslist:
                    if(car.get('id') == int(ids)):
                        car['color'] = colors[int(ids)-1]
                        selected_cars.append(car)
            post_data['selected_cars'] = json.dumps(selected_cars)
            # print('post data being sent')
            # print(post_data['selected_cars'])
        if 'selected_motorcycles' in request.POST.keys():
            selected_motorcycles = []
            # print('to aqui')
            # print(request.POST.getlist('selected_motorcycles'))
            for ids in request.POST.getlist('selected_motorcycles'):
                for motorcycle in motorcyclelist:
                    if(motorcycle.get('id') == int(ids)):
                        selected_motorcycles.append(motorcycle)
            
            post_data['selected_motorcycles'] = json.dumps(selected_motorcycles)
            # print('post data being sent')
            # print(post_data['selected_motorcycles'])

        
        is_updated = requests.post(url_updategatage, data = post_data)
        # print(is_updated.json().get('updating_garage'))
    
    result = requests.post(url_getUserGarage,data = {'email': user_email})
    actual_user_garage = result.json().get('garage')
    if(actual_user_garage == 'Your Garage is empty'):
        return render(request, 'home.html',{'carlist':carslist, 'motorcyclelist':motorcyclelist, 'colors':colorlist, 'user_garage':actual_user_garage,'is_empty': True})
    else:
        return render(request, 'home.html',{'carlist':carslist, 'motorcyclelist':motorcyclelist, 'colors':colorlist, 'user_garage':actual_user_garage, 'is_empty':False})


def infoAndDelete(request):
    url_deleteGarage = 'http://localhost:5000/api/delete-vehicle/'
    url_checkGarageItem = 'http://localhost:5000/api/garage-item/'
    logged_in_user = auth.get_user(request)
    user_email = models.User.objects.filter(username=logged_in_user).values_list('email', flat=True)
    print('checking out the info and Delete url')
    post_data = {'email': user_email}
    if request.method == 'POST':
        if 'infobtn' in request.POST.keys():
            #print('checking out the info and Delete url')
            #print(request.POST)
            post_data['vehicleid'] = request.POST['infobtn']
            res = requests.post(url_checkGarageItem, data = post_data)
            #print(res.json().get('car_details')[0])
            return render(request,'home.html',{'car_details':res.json().get('car_details')[0]})
            

        if 'deletenbtn' in request.POST.keys():
            post_data['vehicleid'] = request.POST['deletenbtn']
            requests.post(url_deleteGarage, data = post_data)
    
    return redirect('/home')   


@api_view(['GET'])
def getEverything(request):
    url_allobjects = 'http://localhost:5000/api/all-active-garage'
    all_registered_users = models.User.objects.all()
    serialized_users = UserSerializer(all_registered_users,many=True)
    res = requests.get(url_allobjects)
    #print(res)
    all_active_garages = res.json().get('activeGarage')
    clients_with_Car = []
    for client in all_active_garages:
        clients_with_Car.append(client['email'])
    response = {'all_clients': serialized_users.data, 'active_garages': all_active_garages, 'clients_with_cars':clients_with_Car}
    return Response(response)