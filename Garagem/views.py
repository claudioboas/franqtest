from django.shortcuts import render
from django.http import JsonResponse


from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import GarageOwnerSerializer, GarageOwnerPopulatedSerializer
from .models import GarageOwner
import json

#Uncomment print to checkout the console logs used to develop the application
vehiclemodels = [
        {'type':'car','model':'Audi A4','year':'2018','id': 1},
        {'type':'car','model':'Chery Tiggo 5','year':'2015', 'id': 2},
        {'type':'car','model':'Chevrolet Malibu','year':'2014', 'id': 3},
        {'type':'car','model':'Fiat Punto','year':'2019', 'id': 4},
        {'type':'car','model':'Honda Civic','year':'2010', 'id': 5},
        {'type':'car','model':'Tesla Model X','year':'2021', 'id': 6},
        {'type':'car','model':'Mercedes-Benz A-Class','year':'2020', 'id': 7},
        {'type':'car','model':'Honda City','year':'2017', 'id': 8},
        {'type':'car','model':'Mitsubishi Lancer','year':'2019', 'id': 9},
        {'type':'car','model':'Mitsubishi Outlander ','year':'2020', 'id': 10},
        {'type':'car','model':'Nissan Sentra','year':'2016', 'id': 11},
        {'type':'car','model':'Peugeot 4008','year':'2020', 'id': 12},
        {'type':'car','model':'Toyota Corolla','year':'2018', 'id': 13},
        {'type':'car','model':'Toyota Yaris','year':'2019', 'id': 14},
        {'type':'car','model':'Tesla Model Y','year':'2021', 'id': 15},
        {'type':'car','model':'Tesla Model 3','year':'2021', 'id': 16},
        {'type':'motorcycle','model':'BMW R75','year':'2018', 'id': 17},
        {'type':'motorcycle','model':'Ducati Diavel','year':'2017', 'id': 18},
        {'type':'motorcycle','model':'Honda CBR 250','year':'2015', 'id': 19},
        {'type':'motorcycle','model':'Biz 125','year':'2020', 'id': 20},
        {'type':'motorcycle','model':'Zero SR/F','year':'2021', 'id': 21}
    ]
colors = [ 'black','yellow','red','white','silver','blue','purple'] 
@api_view(['POST'])
def garageCreate(request):
    # the Post decorator already forbiddens get request in this function
    # But anyways I consider a good practice reforcing it with the POST if
     if request.method == 'POST':

        #print(request)
        email = request.POST['email']
        phonumber = request.POST['cellPhone']
        user = GarageOwner(email = email , number = phonumber)
        user.save()
        #print('garage created')
        serializer = GarageOwnerSerializer(user, many = False)
        return Response(serializer.data)


@api_view(['GET'])
def vehiclescolorslist(request):
    if request.method == 'GET':
        response = {'vehicles': vehiclemodels, 'colors': colors}
        return Response(response)

@api_view(['POST'])
def userGarage(request):
    if request.method == 'POST':
        user = GarageOwner.objects.get(email=request.POST['email'])
        response = {'garage' : user.get_carInGarage()}
        return Response(response)

@api_view(['POST'])
def delteGarageitem(request):
    if request.method == 'POST':
        user = GarageOwner.objects.get(email=request.POST['email'])
        user_garage = user.get_carInGarage()
        res = [i for i in user_garage if not (i['id'] == int(request.POST['vehicleid']))] 
        #print('the list without your undesired item')
        #print(res)
        user.set_carInGarage(res)
        user.save()
        response = {'updating_garage': 'uhul'}
        return Response(response)


@api_view(['POST'])
def getGarageitem(request):
    if request.method == 'POST':
        user = GarageOwner.objects.get(email=request.POST['email'])
        user_garage = user.get_carInGarage()
        res = [i for i in user_garage if (i['id'] == int(request.POST['vehicleid']))] 
        response = {'car_details': res}
        return Response(response)

@api_view(['POST'])
def updateGarage(request):
    user = GarageOwner.objects.get(email=request.POST['email'])
    user_garage = user.get_carInGarage()
    new_user_garage = []
    is_updatable = False
    if request.method == 'POST':
        if 'selected_cars' in request.POST.keys():
            cars_selected = json.loads(request.POST.getlist('selected_cars')[-1])
            is_updatable = True
            if(user_garage != 'Your Garage is empty'):
                car_is_at_garage = False
                for new_car in cars_selected:
                    car_is_at_garage = False
                    for in_place_cars in user_garage:
                        if(in_place_cars.get('id') == new_car.get('id')):
                            car_is_at_garage = True
                    if(not car_is_at_garage ):
                        user_garage.append(new_car)
            else:
                # print('Updating garage with selected cars')
                selected_cars = request.POST.getlist('selected_cars') 
                # print('before json conversion')
                # print(selected_cars)
                new_user_garage = json.loads(selected_cars[-1])
                # print(len(new_user_garage))
                # print(new_user_garage)


        if 'selected_motorcycles' in request.POST.keys():
            # print('starting motorcycles')
            # print(request.POST.getlist('selected_motorcycles')[-1])
            motorcycles_selected = json.loads(request.POST.getlist('selected_motorcycles')[-1])
            is_updatable = True
            if(user_garage != 'Your Garage is empty'):
                motorcycle_is_at_garage = False
                for new_motorcycle in motorcycles_selected:
                    motorcycle_is_at_garage = False
                    for in_place_vehicles in user_garage:
                        if(in_place_vehicles.get('id') == new_motorcycle.get('id')):
                            motorcycle_is_at_garage = True
                    if(not motorcycle_is_at_garage ):
                        user_garage.append(new_motorcycle)
            else:
                if new_user_garage:
                    for item in motorcycles_selected:
                        new_user_garage.append(item)
                else:
                     new_user_garage = motorcycles_selected
        
        
        if(is_updatable):
            if(user_garage != 'Your Garage is empty'):
                user.set_carInGarage(user_garage)
                user.save()
            else:
                # print('setting new garage parameters')
                user.set_carInGarage(new_user_garage)
                user.save()

        
        response = {'updating_garage': 'uhul'}
        return Response(response)

@api_view(['GET'])
def getEverything(request):
    all_garages = GarageOwner.objects.all()
    res = []
    [res.append(garage) for garage in all_garages if not (garage.get_carInGarage() == 'Your Garage is empty')] 
    #print(res)
    resSerialized = GarageOwnerPopulatedSerializer(res,many=True)
    #print(resSerialized)
    response = {'activeGarage':resSerialized.data}
    return Response(response)