
# serializers.py
from rest_framework import serializers

from .models import GarageOwner

class GarageOwnerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GarageOwner
        fields = ('email', 'number')


class GarageOwnerPopulatedSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GarageOwner
        fields = ('email', 'number','carsInGarage')