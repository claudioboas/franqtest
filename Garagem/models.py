from django.db import models
import json

# Create your models here.
class GarageOwner(models.Model):
    email = models.CharField(max_length=115)
    number = models.CharField(max_length=115)
    carsInGarage = models.CharField(max_length=2000,default="")
    def __str__(self):
        return self.email

    def set_carInGarage(self, x):
        self.carsInGarage = json.dumps(x)
    def get_carInGarage(self):
        if(self.carsInGarage):
            return json.loads(self.carsInGarage)
        else:
            return 'Your Garage is empty'