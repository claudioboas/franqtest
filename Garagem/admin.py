from django.contrib import admin
from .models import GarageOwner

# Register your models here.
admin.site.register(GarageOwner)
