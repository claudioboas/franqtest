from django.urls import path
from . import views


urlpatterns = [
    path('garage-create/', views.garageCreate, name="garage-Create"),
    path('vehicles-list/', views.vehiclescolorslist, name="vehicles-list"),
    path('update-garage/', views.updateGarage, name="update-garage"),
    path('user-garage/', views.userGarage, name="user-garage"),
    path('delete-vehicle/', views.delteGarageitem, name="delete-vehicle"),
    path('garage-item/', views.getGarageitem, name="garage-item"),
    path('all-active-garage/', views.getEverything, name="all-active-garage"),
]